/*!
 *\file var.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of var node class.
 */

#ifndef VAR_H
#define VAR_H

#include <string>
#include "node.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Var node class.
             */
            class Var : public Node
            {
            public:
                Var(const std::string& var);
                void accept(Visitor& visitor) const override;
                std::string var() const;

            private:
                std::string _var;
            };
        }
    }
}

#endif // VAR_H
