/*!
 *\file binaryoperation.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of binary operation node abstract class.
 */

#ifndef BINARYOPERATION_H
#define BINARYOPERATION_H

#include "node.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Abstract binary operation class.
             */
            class BinaryOperation : public Node
            {
            public:
                /*!
                 * Construct binary operation.
                 *\param[in] left Left child.
                 *\param[in] right Right child.
                 */
                BinaryOperation(const Node* left, const Node* right);
                virtual ~BinaryOperation() = 0;

                /*!
                 * Get left child.
                 */
                const Node* leftNode() const;

                /*!
                 * Get right child.
                 */
                const Node* rightNode() const;

            private:
                const Node* _left;
                const Node* _right;
            };
        }
    }
}

#endif // BINARYOPERATION_H
