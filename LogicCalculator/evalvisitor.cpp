#include "evalvisitor.h"

melihovv::LogicCalc::Ast::EvalVisitor::EvalVisitor(
    const Context& context /*= Context()*/
    )
{
    _context = context;
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(const Number* number)
{
    _result = number->number();
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(const Var* var)
{
    _result = _context.at(var->var());
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(const Negation* negation)
{
    negation->child()->accept(*this);
    _result = _result == false ? true : false;
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(const Equivalence* equiv)
{
    std::tuple<bool, bool> t = visitBinaryOperation(equiv);
    bool l = std::get<0>(t);
    bool r = std::get<1>(t);
    _result = (!l || r) && (l || !r);
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(
    const Implication* implication
    )
{
    std::tuple<bool, bool> t = visitBinaryOperation(implication);
    bool l = std::get<0>(t);
    bool r = std::get<1>(t);
    _result = !l || r;
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(
    const Disjunction* disjunction
    )
{
    std::tuple<bool, bool> t = visitBinaryOperation(disjunction);
    _result = std::get<0>(t) || std::get<1>(t);
}

void melihovv::LogicCalc::Ast::EvalVisitor::visit(
    const Conjunction* conjunction
    )
{
    std::tuple<bool, bool> t = visitBinaryOperation(conjunction);
    _result = std::get<0>(t) && std::get<1>(t);
}

bool melihovv::LogicCalc::Ast::EvalVisitor::result() const
{
    return _result;
}

void melihovv::LogicCalc::Ast::EvalVisitor::setContext(
    const Context& context
    )
{
    _context = context;
}

std::tuple<bool, bool>
melihovv::LogicCalc::Ast::EvalVisitor::visitBinaryOperation(
    const BinaryOperation* binaryOperation
)
{
    binaryOperation->leftNode()->accept(*this);
    bool left = _result;
    binaryOperation->rightNode()->accept(*this);
    bool right = _result;
    return std::make_tuple(left, right);
}
