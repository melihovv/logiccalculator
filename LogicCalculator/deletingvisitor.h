/*!
 *\file deletingvisitor.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of deleting visitor class.
 */

#ifndef DELETINGVISITOR_H
#define DELETINGVISITOR_H

#include "visitor.h"
#include "number.h"
#include "var.h"
#include "negation.h"
#include "equivalence.h"
#include "implication.h"
#include "disjunction.h"
#include "conjunction.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Deleting visitor class.
             */
            class DeletingVisitor : public Visitor
            {
            public:
                virtual void visit(const Number* number) override;
                virtual void visit(const Var* var) override;
                virtual void visit(const Negation* negation) override;
                virtual void visit(const Equivalence* equiv) override;
                virtual void visit(const Implication* implication) override;
                virtual void visit(const Disjunction* disjunction) override;
                virtual void visit(const Conjunction* conjunction) override;

            private:
                void visitBinaryOperation(const BinaryOperation* binOperation);
                void visitUnaryOperation(const UnaryOperation* unaryOperation);
                void visitNode(const Node* node);
            };
        }
    }
}

#endif // DELETINGVISITOR_H
