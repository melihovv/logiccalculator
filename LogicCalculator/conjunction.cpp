#include "conjunction.h"

melihovv::LogicCalc::Ast::Conjunction::Conjunction(
    const Node* left,
    const Node* right
    )
    : BinaryOperation(left, right)
{
}

void melihovv::LogicCalc::Ast::Conjunction::accept(
    Visitor& visitor
    ) const
{
    visitor.visit(this);
}
