#include "logiccalc.h"
#include <sstream>
#include <cmath>
#include <QString>
#include <QVector>

bool LogicCalc::parse(const std::string& expr)
{
    std::istringstream stream(expr);
    if (_driver.get() != nullptr)
    {
        delete _driver.release();
    }
    _driver = std::unique_ptr<melihovv::LogicCalc::Driver>(
        new melihovv::LogicCalc::Driver(&stream)
        );
    return _driver->parse();
}

TruthTable LogicCalc::truthTable()
{
    if (_truthTable.size() != 0)
    {
        return _truthTable;
    }

    std::set<std::string> varsNames = _driver->varsNames();
    if (varsNames.size() == 0)
    {
        bool result = _driver->result(melihovv::LogicCalc::Ast::Context());
        if (result)
        {
            _isIdenticallyTrue = true;
            _isDoable = true;
            _isIdenticallyFalse = false;
            _isRebuttable = false;
            _pcnf = '1';
            _pdnf = '1';
        }
        else
        {
            _isIdenticallyTrue = false;
            _isDoable = false;
            _isIdenticallyFalse = true;
            _isRebuttable = true;
            _pcnf = '0';
            _pdnf = '0';
        }

        _truthTable = TruthTable({{result}});
        return _truthTable;
    }

    TruthTable rows;
    int varsNamesSize = varsNames.size();
    int rowsAmount = pow(2, varsNamesSize);

    _isIdenticallyTrue = true;
    bool isntAlwaysFalse = false;

    for (int i = 0; i < rowsAmount; ++i)
    {
        QString binString = QString::number(i, 2);

        if (rowsAmount != varsNamesSize)
        {
            int binStringLength = binString.size();
            int diff = varsNamesSize - binStringLength;
            binString = QString(diff, '0') + binString;
        }

        std::vector<bool> cols;
        if (varsNamesSize != 0)
        {
            for (auto alpha : binString)
            {
                cols.push_back(QString(alpha).toInt());
            }
        }

        int counter = 0;
        melihovv::LogicCalc::Ast::Context context;
        for (auto varName : varsNames)
        {
            std::string s;
            context.insert(
                std::pair<std::string, bool>(varName, cols[counter++])
                );
        }

        bool result = _driver->result(context);
        cols.push_back(result);
        rows.push_back(cols);

        _isIdenticallyTrue = _isIdenticallyTrue && result;
        isntAlwaysFalse = isntAlwaysFalse || result;
    }

    if (_isIdenticallyTrue)
    {
        _isDoable = true;
        _isRebuttable = false;
        _isIdenticallyFalse = false;
    }
    else if (!isntAlwaysFalse)
    {
        _isIdenticallyFalse = true;
        _isRebuttable = true;
        _isIdenticallyTrue = false;
        _isDoable = false;
    }
    else
    {
        _isIdenticallyTrue = false;
        _isIdenticallyFalse = false;
        _isDoable = true;
        _isRebuttable = true;
    }

    _truthTable = rows;
    return rows;
}

std::set<std::string> LogicCalc::headerRow() const
{
    return _driver->varsNames();
}

std::list<melihovv::LogicCalc::Error> LogicCalc::errors() const
{
    return _driver->errors();
}

bool LogicCalc::isIdenticallyTrue() const
{
    return _isIdenticallyTrue;
}

bool LogicCalc::isIdenticallyFalse() const
{
    return _isIdenticallyFalse;
}

bool LogicCalc::isRebuttable() const
{
    return _isRebuttable;
}

bool LogicCalc::isDoable() const
{
    return _isDoable;
}

QString LogicCalc::pcnf()
{
    if (_pcnf != "")
    {
        return _pcnf;
    }

    _pcnf = pnf(0, '|', '&');
    return _pcnf;
}

QString LogicCalc::pdnf()
{
    if (_pdnf != "")
    {
        return _pdnf;
    }

    _pdnf = pnf(1, '&', '|');
    return _pdnf;
}

QString LogicCalc::pnf(int digit, char char1, char char2)
{
    int rowsAmount = _truthTable.size();
    int varsAmount = log2(rowsAmount);
    QString result;

    for (int i = 0; i < rowsAmount; ++i)
    {
        if (_truthTable[i][varsAmount] == digit)
        {
            result += '(';

            QVector<QString> varsNames;
            for (const auto& varName : _driver->varsNames())
            {
                varsNames.push_back(QString::fromStdString(varName));
            }
            
            int length = _truthTable[i].size();
            for (int j = 0; j < length - 1; ++j)
            {
                if (_truthTable[i][j] != digit)
                {
                    result += '!';
                }
                result += varsNames[j];

                if (j != length - 2)
                {
                    result += char1;
                }
            }

            result += ')';

            if (i != rowsAmount - 1)
            {
                result += char2;
            }
        }
    }

    if (result == "")
    {
        result = '1';
    }
    else
    {
        if (result[result.size() - 1] == char2)
        {
            result.remove(result.size() - 1, 1);
        }
    }
    return result;
}
