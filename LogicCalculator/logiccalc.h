/*!
 *\file logiccalc.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of logical calculator class.
 */
#ifndef LOGICCALC_H
#define LOGICCALC_H

#include "driver.h"
#include <vector>
#include <memory>
#include "error.h"
#include <QString>

typedef std::vector<std::vector<bool>> TruthTable;

/*!
 * Logical calculator class.
 */
class LogicCalc
{
public:
    /*!
     * Parse logical expression.
     *\param[in] expr Logical expression.
     */
    bool parse(const std::string& expr);

    /*!
     * Get truth table.
     *\return Truth table.
     */
    TruthTable truthTable();

    /*!
     * Get header row.
     *\return Header row.
     */
    std::set<std::string> headerRow() const;

    /*!
     * Get syntax errors.
     *\return List of syntax errors.
     */
    std::list<melihovv::LogicCalc::Error> errors() const;

    /*!
     * Return true if logical function is identically-true.
     */
    bool isIdenticallyTrue() const;

    /*!
     * Return true if logical function is identically-false.
     */
    bool isIdenticallyFalse() const;

    /*!
     * Return true if logical function is rebuttable.
     */
    bool isRebuttable() const;

    /*!
     * Return true if logical function is doable.
     */
    bool isDoable() const;

    /*!
     * Get perfect conjunctive normal form.
     */
    QString pcnf();

    /*!
     * Get perfect disjunctive normal form.
     */
    QString pdnf();

    /*!
     * Get perfect normal form.
     */
    QString pnf(int digit, char char1, char char2);

private:
    std::unique_ptr<melihovv::LogicCalc::Driver> _driver;
    TruthTable _truthTable;
    bool _isIdenticallyTrue;
    bool _isIdenticallyFalse;
    bool _isRebuttable;
    bool _isDoable;
    QString _pcnf = "";
    QString _pdnf = "";
};

#endif // LOGICCALC_H
