/*!
 *\file mainwindow.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of main window class.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QMainWindow>
#include "ui_mainwindow.h"
#include "logiccalc.h"

/*!
 * Main window class.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:
    /*!
     * Key pressed event handler.
     */
    void onEnterPressed();

private:
    /*!
     * Connect signals with slots.
     */
    void setConnections();

    /*!
     * Render table.
     *\param[in] table Truth table.
     *\param[in] header Header of table.
     */
    void renderTable(const TruthTable& table,
        const std::set<std::string>& header);

    /*!
     * Clear table.
     */
    void clearTable();

    /*!
     * Add header to table.
     *\param[in] headers Header row columns.
     */
    void addHeader(const std::set<std::string>& headers);

    /*!
     * Add row to table.
     *\param[in] cols Columns.
     */
    void addRow(const std::vector<bool>& cols);

    Ui::MainWindowClass ui;
};

#endif // MAINWINDOW_H
