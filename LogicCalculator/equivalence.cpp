#include "equivalence.h"

melihovv::LogicCalc::Ast::Equivalence::Equivalence(
    const Node* left,
    const Node* right
    )
    : BinaryOperation(left, right)
{
}

void melihovv::LogicCalc::Ast::Equivalence::accept(Visitor& visitor) const
{
    visitor.visit(this);
}
