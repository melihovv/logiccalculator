#include "number.h"

melihovv::LogicCalc::Ast::Number::Number(int number)
{
    _number = number;
}

void melihovv::LogicCalc::Ast::Number::accept(Visitor& visitor) const
{
    visitor.visit(this);
}

int melihovv::LogicCalc::Ast::Number::number() const
{
    return _number;
}
