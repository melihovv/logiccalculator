#include "error.h"

melihovv::LogicCalc::Error::Error(const LogicCalc::location& location,
    const std::string& message)
{
    _location = location;
    _message = message;
}

std::string melihovv::LogicCalc::Error::error() const
{
    std::ostringstream stream;
    stream << _location << ": " << _message;
    return stream.str();
}
