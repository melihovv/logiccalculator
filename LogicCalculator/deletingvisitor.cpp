#include "deletingvisitor.h"

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(const Number* number)
{
    visitNode(number);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(const Var* var)
{
    visitNode(var);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(const Negation* negation)
{
    visitUnaryOperation(negation);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(const Equivalence* equiv)
{
    visitBinaryOperation(equiv);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(
    const Implication* implication
    )
{
    visitBinaryOperation(implication);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(
    const Disjunction* disjunction
    )
{
    visitBinaryOperation(disjunction);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visit(
    const Conjunction* conjunction
    )
{
    visitBinaryOperation(conjunction);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visitBinaryOperation(
    const BinaryOperation* binOperation
    )
{
    binOperation->leftNode()->accept(*this);
    binOperation->rightNode()->accept(*this);
    visitNode(binOperation);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visitUnaryOperation(
    const UnaryOperation* unaryOperation
    )
{
    unaryOperation->child()->accept(*this);
    visitNode(unaryOperation);
}

void melihovv::LogicCalc::Ast::DeletingVisitor::visitNode(const Node* node)
{
    delete node;
    node = nullptr;
}
