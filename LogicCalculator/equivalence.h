/*!
 *\file equivalence.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of equivalence node class.
 */

#ifndef EQUIVALENCE_H
#define EQUIVALENCE_H

#include "binaryoperation.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Equivalence node class.
             */
            class Equivalence : public BinaryOperation
            {
            public:
                Equivalence(const Node* left, const Node* right);
                void accept(Visitor& visitor) const override;
            };
        }
    }
}

#endif // EQUIVALENCE_H
