/*!
 *\file driver.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of logic calculator driver class.
 */

#ifndef DRIVER_H
#define DRIVER_H

#include <string>
#include <list>
#include <memory>
#include <set>
#include "node.h"
#include "error.h"
#include "location.hh"
#include "parser.tab.hh"
#include "evalvisitor.h"
#include "deletingvisitor.h"

namespace melihovv {
    namespace LogicCalc {

        /*!
         * Driver class.
         */
        class Driver
        {
        public:
            Driver(std::istream* is = 0);
            ~Driver();

            /*!
             * Parse input.
             *\return Result of parsing.
             */
            bool parse();

            /*!
             * Get result of logical expression.
             *\param[in] context Context.
             *\return Result of logical expression.
             */
            bool result(const Ast::Context& context = Ast::Context());

            /*!
             * Get vars names.
             *\return Vars names.
             */
            std::set<std::string> varsNames() const;

            /*!
             * Switch lexer input stream. Default is standard input
             * (std::cin).
             *\param[in] is Input stream.
             */
            void switchInputStream(std::istream* is);

            /*!
             * Set name of parsing file.
             *\param[in] fileName Name of parsing file.
             */
            void setFileName(const std::string& fileName);

            /*!
             * Get syntax errors.
             *\return List of syntax errors.
             */
            std::list<Error> errors() const;

            friend class Parser;
            friend class Lexer;

        private:
            /*!
             * Add error in list of errors.
             *\param[in] location Location of error.
             *\param[in] message Error message.
             */
            void addError(
                const location& location,
                const std::string& message
                );

            std::string _fileName = "";
            std::list<Error> _errors;
            const Ast::Node* _root = nullptr;
            Ast::EvalVisitor _evalVisitor;
            Ast::DeletingVisitor _delVisitor;
            std::unique_ptr<Lexer> _lexer;
            std::unique_ptr<Parser> _parser;
            std::set<std::string> _varsNames;
        };
    }
}

#endif // DRIVER_H
