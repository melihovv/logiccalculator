#include "unaryoperation.h"

melihovv::LogicCalc::Ast::UnaryOperation::UnaryOperation(
    const Node* child
    )
{
    _child = child;
}

melihovv::LogicCalc::Ast::UnaryOperation::~UnaryOperation()
{
}

const melihovv::LogicCalc::Ast::Node*
melihovv::LogicCalc::Ast::UnaryOperation::child() const
{
    return _child;
}
