#include "negation.h"

melihovv::LogicCalc::Ast::Negation::Negation(const Node* child)
    : UnaryOperation(child)
{
}

void melihovv::LogicCalc::Ast::Negation::accept(
    Visitor& visitor
    ) const
{
    visitor.visit(this);
}
