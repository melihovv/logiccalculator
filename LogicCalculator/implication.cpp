#include "implication.h"

melihovv::LogicCalc::Ast::Implication::Implication(
    const Node* left,
    const Node* right
    )
    : BinaryOperation(left, right)
{
}

void melihovv::LogicCalc::Ast::Implication::accept(Visitor& visitor) const
{
    visitor.visit(this);
}
