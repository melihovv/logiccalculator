/*!
 *\file disjunction.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of disjunction node class.
 */

#ifndef DISJUNCTION_H
#define DISJUNCTION_H

#include "binaryoperation.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Disjunction node class.
             */
            class Disjunction : public BinaryOperation
            {
            public:
                Disjunction(const Node* left, const Node* right);
                void accept(Visitor& visitor) const override;
            };
        }
    }
}

#endif // DISJUNCTION_H
