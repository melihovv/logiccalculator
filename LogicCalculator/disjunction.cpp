#include "disjunction.h"

melihovv::LogicCalc::Ast::Disjunction::Disjunction(
    const Node* left,
    const Node* right
    )
    : BinaryOperation(left, right)
{
}

void melihovv::LogicCalc::Ast::Disjunction::accept(
    Visitor& visitor
    ) const
{
    visitor.visit(this);
}
