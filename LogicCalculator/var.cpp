#include "var.h"

melihovv::LogicCalc::Ast::Var::Var(const std::string& var)
{
    _var = var;
}

void melihovv::LogicCalc::Ast::Var::accept(
    Visitor& visitor
    ) const
{
    visitor.visit(this);
}

std::string melihovv::LogicCalc::Ast::Var::var() const
{
    return _var;
}
