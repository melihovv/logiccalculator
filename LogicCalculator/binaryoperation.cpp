#include "binaryoperation.h"

melihovv::LogicCalc::Ast::BinaryOperation::BinaryOperation(
    const Node* left,
    const Node* right
    )
{
    _left = left;
    _right = right;
}

melihovv::LogicCalc::Ast::BinaryOperation::~BinaryOperation()
{
}

const melihovv::LogicCalc::Ast::Node*
melihovv::LogicCalc::Ast::BinaryOperation::leftNode() const
{
    return _left;
}

const melihovv::LogicCalc::Ast::Node*
melihovv::LogicCalc::Ast::BinaryOperation::rightNode() const
{
    return _right;
}
