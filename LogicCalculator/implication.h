/*!
 *\file implication.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of implication node class.
 */

#ifndef IMPLICATION_H
#define IMPLICATION_H

#include "binaryoperation.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Implication node class.
             */
            class Implication : public BinaryOperation
            {
            public:
                Implication(const Node* left, const Node* right);
                void accept(Visitor& visitor) const override;
            };
        }
    }
}

#endif // IMPLICATION_H
