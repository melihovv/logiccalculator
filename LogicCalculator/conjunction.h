/*!
 *\file conjunction.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of conjunction node class.
 */

#ifndef CONJUNCTION_H
#define CONJUNCTION_H

#include "binaryoperation.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            /*!
             * Conjunction node class.
             */
            class Conjunction : public BinaryOperation
            {
            public:
                Conjunction(const Node* left, const Node* right);
                void accept(Visitor& visitor) const override;
            };
        }
    }
}

#endif // CONJUNCTION_H
