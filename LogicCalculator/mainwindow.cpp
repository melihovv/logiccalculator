#include "mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    setConnections();
    ui.truthTable->verticalHeader()->setVisible(false);
}

MainWindow::~MainWindow()
{
}

void MainWindow::onEnterPressed()
{
    clearTable();
    std::string input = ui.input->text().toStdString();

    if (input.length() > 0)
    {
        LogicCalc calc;
        if (calc.parse(ui.input->text().toStdString()))
        {
            renderTable(calc.truthTable(), calc.headerRow());

            QString funcType;
            if (calc.isIdenticallyTrue())
            {
                funcType = "тождественно-истинная, выполнимая";
            }
            else if (calc.isIdenticallyFalse())
            {
                funcType = "тождественно-ложная, опровержимая";
            }
            else
            {
                funcType = "выполнимая, опровержимая";
            }
            ui.funcType->setText(funcType);

            ui.pcnf->setText(calc.pcnf());
            ui.pdnf->setText(calc.pdnf());
        }
        else
        {
            QMessageBox::critical(
                this,
                "Ошибка ввода",
                QString::fromStdString(calc.errors().front().error())
                );
        }
    }
}

void MainWindow::setConnections()
{
    connect(
        ui.input,
        SIGNAL(returnPressed()),
        this,
        SLOT(onEnterPressed())
        );
}

void MainWindow::renderTable(const TruthTable& table,
    const std::set<std::string>& header)
{
    ui.truthTable->setColumnCount(table.size() > 0 ? table[0].size() : 0);

    addHeader(header);
    for (auto row : table)
    {
        addRow(row);
    }
}

void MainWindow::clearTable()
{
    ui.truthTable->clear();
    ui.truthTable->setRowCount(0);
    ui.truthTable->setColumnCount(0);
}

void MainWindow::addHeader(const std::set<std::string>& headers)
{
    int row = 0;
    int counter = 0;

    for (auto h : headers)
    {
        ui.truthTable->setHorizontalHeaderItem(
            counter++,
            new QTableWidgetItem(QString::fromStdString(h))
            );
    }

    ui.truthTable->setHorizontalHeaderItem(
        counter++,
        new QTableWidgetItem("Result")
        );

    ui.truthTable->setCurrentCell(row, 0);
}

void MainWindow::addRow(const std::vector<bool>& cols)
{
    int row = ui.truthTable->rowCount();
    ui.truthTable->insertRow(row);

    int counter = 0;
    for (auto v : cols)
    {
        QTableWidgetItem* item = new QTableWidgetItem(
            QString::number(v ? 1 : 0)
            );
        item->setTextAlignment(Qt::AlignCenter);
        ui.truthTable->setItem(row, counter++, item);
    }

    ui.truthTable->setCurrentCell(row, 0);
}
