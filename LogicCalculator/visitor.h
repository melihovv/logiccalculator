/*!
 *\file visitor.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of visitor abstract class.
 */

#ifndef VISITOR_H
#define VISITOR_H

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            class Node;
            class Number;
            class Var;
            class Negation;
            class Equivalence;
            class Implication;
            class Disjunction;
            class Conjunction;

            /*!
             * Visitor class.
             */
            class Visitor
            {
            public:
                virtual ~Visitor() = 0;

                /*!
                 * Visit number node.
                 *\param[in] number Number node.
                 */
                virtual void visit(const Number* number) = 0;

                /*!
                 * Visit var node.
                 *\param[in] var Var node.
                 */
                virtual void visit(const Var* var) = 0;

                /*!
                 * Visit negation node.
                 *\param[in] negation Negation node.
                 */
                virtual void visit(const Negation* negation) = 0;

                /*!
                 * Visit equivalence node.
                 *\param[in] equiv Equivalence node.
                 */
                virtual void visit(const Equivalence* equiv) = 0;

                /*!
                 * Visit addition node.
                 *\param[in] addition Implication node.
                 */
                virtual void visit(const Implication* implication) = 0;

                /*!
                 * Visit disjunction node.
                 *\param[in] disjunction Disjunction node.
                 */
                virtual void visit(const Disjunction* disjunction) = 0;

                /*!
                 * Visit conjunction node.
                 *\param[in] conjunction Conjunction node.
                 */
                virtual void visit(const Conjunction* conjunction) = 0;
            };
        }
    }
}

#endif // VISITOR_H
