/*!
 *\file evalvisitor.h
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of evaluating visitor class.
 */

#ifndef EVALVISITOR_H
#define EVALVISITOR_H

#include <cmath>
#include <tuple>
#include <string>
#include <map>
#include "visitor.h"
#include "number.h"
#include "var.h"
#include "negation.h"
#include "equivalence.h"
#include "implication.h"
#include "disjunction.h"
#include "conjunction.h"

namespace melihovv {
    namespace LogicCalc {
        namespace Ast {

            typedef std::map<std::string, bool> Context;

            /*!
             * Evaluating visitor class.
             */
            class EvalVisitor : public Visitor
            {
            public:
                EvalVisitor(const Context& context = Context());

                virtual void visit(const Number* number) override;

                /*!
                 *\throws When there are no variable with such name in context.
                 */
                virtual void visit(const Var* var) throw(std::out_of_range)
                    override;
                virtual void visit(const Negation* negation) override;
                virtual void visit(const Equivalence* equiv) override;
                virtual void visit(const Implication* implication) override;
                virtual void visit(const Disjunction* disjunction) override;
                virtual void visit(const Conjunction* conjunction) override;

                /*!
                 * Get evaluating result.
                 */
                bool result() const;

                /*!
                 * Set context.
                 */
                void setContext(const Context& context);

            private:
                /*!
                 * Visit binary operation.
                 *\param binaryOperation Binary operation.
                 *\return Tuple of results of visiting left and right nodes.
                 */
                std::tuple<bool, bool> visitBinaryOperation(
                    const BinaryOperation* binaryOperation
                    );

                bool _result = 0;
                Context _context;
            };
        }
    }
}

#endif // EVALVISITOR_H
