#include "driver.h"
#include "lexer.h"

melihovv::LogicCalc::Driver::Driver(std::istream* is /*= 0*/)
{
    _lexer = std::unique_ptr<Lexer>(new Lexer(is));
    _parser = std::unique_ptr<Parser>(new Parser(*_lexer.get(), *this));
}

melihovv::LogicCalc::Driver::~Driver()
{
    if (_root != nullptr)
    {
        _root->accept(_delVisitor);
    }
}

bool melihovv::LogicCalc::Driver::parse()
{
    _errors.clear();
    _parser->parse();

    if (_errors.size() != 0)
    {
        return false;
    }

    return true;
}

bool melihovv::LogicCalc::Driver::result(
    const Ast::Context& context /*= Ast::Context()*/
    )
{
    _evalVisitor.setContext(context);
    _root->accept(_evalVisitor);
    return _evalVisitor.result();
}

std::set<std::string> melihovv::LogicCalc::Driver::varsNames() const
{
    return _varsNames;
}

void melihovv::LogicCalc::Driver::switchInputStream(std::istream* is)
{
    _lexer->switch_streams(is);
}

void melihovv::LogicCalc::Driver::setFileName(const std::string& fileName)
{
    this->_fileName = fileName;
}

std::list<melihovv::LogicCalc::Error>
melihovv::LogicCalc::Driver::errors() const
{
    return _errors;
}

void melihovv::LogicCalc::Driver::addError(
    const location& location,
    const std::string& message
    )
{
    _errors.push_back(Error(location, message));
}
