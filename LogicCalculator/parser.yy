/*!
 *\file parser.yy
 *\author Alexander Melihov <amelihovv@ya.ru>
 *\version 0.0.1
 *
 *\license This code is distributed under the very permissive MIT License but,
 * if you use it, you might consider referring to the repository.
 *
 * The file contains declaration of syntax analyzer.
 */

/*! Require bison >=3.0.4. */
%require "3.0.4"

/*! Use c++ skeleton file. */
%skeleton "lalr1.cc"

/*! Generate header file. */
%defines

/*! For parser trace. Disable this for release version. */
%debug

/*! Verbose error messages. */
%error-verbose

/*! Enable location tracking. */
%locations
%initial-action
{
    @$.begin.filename = @$.end.filename = &driver._fileName;
};

%define api.namespace {melihovv::LogicCalc}
%define parser_class_name {Parser}
%parse-param {melihovv::LogicCalc::Lexer& lexer}
%parse-param {melihovv::LogicCalc::Driver& driver}
%lex-param {melihovv::LogicCalc::Driver& driver}

%code requires {
    #include <string>
    #include <list>
    #include "driver.h"
    #include "error.h"
    #include "number.h"
    #include "var.h"
    #include "equivalence.h"
    #include "implication.h"
    #include "disjunction.h"
    #include "conjunction.h"
    #include "negation.h"

    namespace melihovv {
        namespace LogicCalc {
            class Lexer;
            class Error;
            class Driver;

            namespace Ast {
                class Number;
                class Var;
                class Negation;
                class Equivalence;
                class Implication;
                class Disjunction;
                class Conjunction;
            }
        }
    }
}

%code {
    #include "lexer.h"

    #define yylex(x) lexer.mylex(x)

    void melihovv::LogicCalc::Parser::error(
            const location_type& location,
            const std::string& message
        )
    {
        driver.addError(location, message);
    }
}

/*! Generate constructor functions for tokens. */
%define api.token.constructor

/*! Use variants instead of union. */
%define api.value.type variant

/*! Issue runtime assertions to catch invalid uses. */
//%define parse.assert

%token
    END 0 "end of file"
    EQUIV
    IMPL
    DISJ "|"
    CONJ "&"
    LEFT_PAREN "("
    RIGHT_PAREN ")"
;

%token <std::string> VAR;
%token <int> NUMBER;
%token <int> NEG;

%left EQUIV
%right IMPL
%left DISJ
%left CONJ
%precedence NEG
%type <Ast::Node*> Exp;

%start Input

%%
Input
    : Exp {driver._root = $1;}
    ;

Exp
    : NUMBER {$$ = new Ast::Number($1);}
    | VAR {$$ = new Ast::Var($1); driver._varsNames.insert($1);}
    | Exp EQUIV Exp {$$ = new Ast::Equivalence($1, $3);}
    | Exp IMPL Exp {$$ = new Ast::Implication($1, $3);}
    | Exp DISJ Exp {$$ = new Ast::Disjunction($1, $3);}
    | Exp CONJ Exp {$$ = new Ast::Conjunction($1, $3);}
    | NEG Exp {$1 % 2 != 0 ? $$ = new Ast::Negation($2) : $$ = $2;}
    | "(" Exp ")" {$$ = $2;}
    ;
%%
