#include "testlogiccalc.h"
#include "../LogicCalculator/driver.h"
#include <sstream>
#include <QMap>

void TestLogicCalc::testDriver_data()
{
    QTest::addColumn<QString>("expr");
    QTest::addColumn<Context>("context");
    QTest::addColumn<bool>("expectation");

    QTest::newRow("constants:true")
        << "1"
        << Context()
        << true;

    QTest::newRow("constants:false")
        << "0"
        << Context()
        << false;

    QTest::newRow("constants: simple expression")
        << "0|1"
        << Context()
        << true;

    QTest::newRow("substitution variable with its value:true")
        << "A"
        << Context({{"A", true}})
        << true;

    QTest::newRow("substitution variable with its value:false")
        << "A"
        << Context({{"A", false}})
        << false;

    QTest::newRow("negation:constant")
        << "!0"
        << Context()
        << true;

    QTest::newRow("negation: even number of exclamtion points")
        << "!!0"
        << Context()
        << false;

    QTest::newRow("negation:var")
        << "!A"
        << Context({{"A", true}})
        << false;

    QTest::newRow("disjunction: 0|0")
        << "A|B"
        << Context({{"A", false}, {"B", false}})
        << false;

    QTest::newRow("disjunction: 0|1")
        << "A|B"
        << Context({{"A", false}, {"B", true}})
        << true;

    QTest::newRow("disjunction: 1|0")
        << "A|B"
        << Context({{"A", true}, {"B", false}})
        << true;

    QTest::newRow("disjunction: 1|1")
        << "A|B"
        << Context({{"A", true}, {"B", true}})
        << true;

    QTest::newRow("conjunction: 0&0")
        << "A&B"
        << Context({{"A", false}, {"B", false}})
        << false;

    QTest::newRow("conjunction: 0&1")
        << "A&B"
        << Context({{"A", false}, {"B", true}})
        << false;

    QTest::newRow("conjunction: 1&0")
        << "A&B"
        << Context({{"A", true}, {"B", false}})
        << false;

    QTest::newRow("conjunction: 1&1")
        << "A&B"
        << Context({{"A", true}, {"B", true}})
        << true;

    QTest::newRow("implication: 0->0")
        << "A->B"
        << Context({{"A", false}, {"B", false}})
        << true;

    QTest::newRow("implication: 0->1")
        << "A->B"
        << Context({{"A", false}, {"B", true}})
        << true;

    QTest::newRow("implication: 1->0")
        << "A->B"
        << Context({{"A", true}, {"B", false}})
        << false;

    QTest::newRow("implication: 1->1")
        << "A->B"
        << Context({{"A", true}, {"B", true}})
        << true;

    QTest::newRow("equivalence: 0<->0")
        << "A<->B"
        << Context({{"A", false}, {"B", false}})
        << true;

    QTest::newRow("equivalence: 0<->1")
        << "A<->B"
        << Context({{"A", false}, {"B", true}})
        << false;

    QTest::newRow("equivalence: 1<->0")
        << "A<->B"
        << Context({{"A", true}, {"B", false}})
        << false;

    QTest::newRow("equivalence: 1<->1")
        << "A<->B"
        << Context({{"A", true}, {"B", true}})
        << true;

    QTest::newRow("must execute disjuction first and implication second:true")
        << "A->B|C"
        << Context({{"A", true}, {"B", true}, {"C", false}})
        << true;

    QTest::newRow("must execute disjuction first and implication second:false")
        << "A->B|C"
        << Context({{"A", true}, {"B", false}, {"C", false}})
        << false;

    QTest::newRow("must execute conjunction first and disjunction second:false")
        << "A|B&C"
        << Context({{"A", false}, {"B", true}, {"C", false}})
        << false;

    QTest::newRow("must execute complex expressions")
        << "(!!A->B)&(A)"
        << Context({{"A", false}, {"B", true}})
        << false;

    QTest::newRow("implication must be left associative")
        << "A->B->C"
        << Context({{"A", false}, {"B", true}, {"C", true}})
        << true;

    QTest::newRow("equivalence must be left associative")
        << "A<->B<->C"
        << Context({{"A", true}, {"B", false}, {"C", true}})
        << false;
}

void TestLogicCalc::testDriver()
{
    QFETCH(QString, expr);
    QFETCH(Context, context);
    QFETCH(bool, expectation);

    std::istringstream iss(expr.toStdString());
    melihovv::LogicCalc::Driver d(&iss);
    d.parse();
    bool real = d.result(context);
    QString message = QString("\nExpected:\n\"%1\"\n\nReal:\n\"%2\"\n")
        .arg(expectation)
        .arg(real);

    QVERIFY2(expectation == real, message.toStdString().c_str());
}

void TestLogicCalc::testTruthTable_data()
{
    QTest::addColumn<QString>("expr");
    QTest::addColumn<TruthTable>("expectation");

    QTest::newRow("disjunction")
        << "a|b"
        << TruthTable(
            {
                std::vector<bool>({0, 0, 0}),
                std::vector<bool>({0, 1, 1}),
                std::vector<bool>({1, 0, 1}),
                std::vector<bool>({1, 1, 1})
            }
        );

    QTest::newRow("conjunction")
        << "a&b"
        << TruthTable(
            {
                std::vector<bool>({0, 0, 0}),
                std::vector<bool>({0, 1, 0}),
                std::vector<bool>({1, 0, 0}),
                std::vector<bool>({1, 1, 1})
            }
        );

    QTest::newRow("implication")
        << "a->b"
        << TruthTable(
            {
                std::vector<bool>({0, 0, 1}),
                std::vector<bool>({0, 1, 1}),
                std::vector<bool>({1, 0, 0}),
                std::vector<bool>({1, 1, 1})
            }
        );

    QTest::newRow("equivalence")
        << "a<->b"
        << TruthTable(
            {
                std::vector<bool>({0, 0, 1}),
                std::vector<bool>({0, 1, 0}),
                std::vector<bool>({1, 0, 0}),
                std::vector<bool>({1, 1, 1})
            }
        );

    QTest::newRow("constants")
        << "0|1"
        << TruthTable(
            {
                std::vector<bool>{1}
            }
        );
}

void TestLogicCalc::testTruthTable()
{
    QFETCH(QString, expr);
    QFETCH(TruthTable, expectation);

    LogicCalc calc;
    calc.parse(expr.toStdString());
    TruthTable real = calc.truthTable();

    int expSize = expectation.size();
    int realSize = real.size();
    QVERIFY2(expSize == realSize,
             QString("Sizes not equal: %1 %2")
                .arg(expSize)
                .arg(realSize)
                .toStdString().c_str()
            );

    int rowSize = real.size() > 0 ? real[0].size() : 0;
    for (int i = 0; i < realSize; ++i)
    {
        QVERIFY2(expectation[i].size() == real[i].size(),
                 QString("Sizes not equal: %1 %2, i=%3")
                    .arg(expectation[i].size())
                    .arg(real[i].size())
                    .arg(i)
                    .toStdString().c_str()
                );

        for (int j = 0; j < rowSize; ++j)
        {
            QString message = QString("\nExpected:\n\"%1\"\n\nReal:\n\"%2\"\n")
                .arg(expectation[i][j])
                .arg(real[i][j]);
            QVERIFY2(expectation[i][j] == real[i][j],
                message.toStdString().c_str());
        }
    }
}
