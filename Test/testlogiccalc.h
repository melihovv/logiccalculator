#ifndef TESTLOGICCALC_H
#define TESTLOGICCALC_H

#include <QObject>
#include <QTest>
#include "testsrunner.h"
#include "../LogicCalculator/logiccalc.h"

typedef std::map<std::string, bool> Context;
Q_DECLARE_METATYPE(Context);
Q_DECLARE_METATYPE(TruthTable);

class TestLogicCalc : public QObject
{
    Q_OBJECT

private slots:
    void testDriver_data();
    void testDriver();

    void testTruthTable_data();
    void testTruthTable();
};

DECLARE_TEST(TestLogicCalc);

#endif // TESTLOGICCALC_H
